const MAX = 3;
const algemeen_channel_id = '827240172999278592';
const test_channel_id = '697026640802349056'; // for testing purposes
const verloren_materiaal_role_id = '698112710424133683';
const zeg_hallo_channel_id = '775447227916484638';
const jvs_server_id = '690291616769900595';
const staf_channel_id = '693070430541709312';
const apod_channel_id = '813491897926156288';

module.exports = {
    MAX,
    algemeen_channel_id,
    test_channel_id,
    verloren_materiaal_role_id,
    zeg_hallo_channel_id,
    jvs_server_id,
    staf_channel_id,
    apod_channel_id,
};
