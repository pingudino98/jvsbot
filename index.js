require('dotenv').config();
const fs = require('fs');
const request = require('request');
const cron = require('cron');
const xkcd = require('xkcd-api');
const git = require('git-last-commit');

const db = require('./db.js');
const values = require('./values');

const Discord = require('discord.js');
const client = new Discord.Client();

const apodJob = cron.job('0 0 12 * * *', () => displayAPOD());
const dbJob = cron.job('0 0 23 * * *', () => db.clearUserExec());
const pinnedMessageRepeatJob = cron.job(' 0 0 21 * * 3', () => repeatPinnedMessage());
const cleanupVerlorenMateriaalJob = cron.job('0 10 0 * * *', () => cleanupVerlorenMateriaal());
const blacklist_cmd = ['play', 'queue', 'stop'];

apodJob.start();
dbJob.start();
pinnedMessageRepeatJob.start();
cleanupVerlorenMateriaalJob.start();

client.on('ready', () => {
    console.log(`Logged in as ${client.user.tag}!`);
    fs.readFile("files/hash.txt", "utf8", (err, hash) => {
        if (err) return console.error(err);
        git.getLastCommit((err, commit) => {
            if (err) return console.error(err);
            if (commit.shortHash !== hash) {
                client.channels.fetch(values.test_channel_id)
                    .then(channel => {
                        channel.send(`Deployed commit ${commit.shortHash} with message "${commit.subject}"
                        by author ${commit.author.name} (${commit.author.email})`);
                        fs.writeFile('files/hash.txt', commit.shortHash, (err) => {
                            if (err) console.error(err);
                        });
                    })
                    .catch(err => console.error(err));
            }
        });
    });
});

client.on('message', async msg => {
    if (!msg.author.bot && msg.content.substring(0, 1) === '!') {
        if (!(await isSTAF(msg))) {
            db.getUserExecutions(msg.author.id).then(async res => {
                if (res.length === 0) {
                    db.insertUser(msg.author.id).catch(err => console.error(err));
                    await execCmd(msg);
                } else {
                    if (res[0].executions < values.MAX) {
                        await execCmd(msg);
                    } else if (res[0].executions === values.MAX) {
                        msg.reply(`Je gebruikte de JVSBot al ${res[0].executions} keer.
                    Ga buiten naar de zon, de wolken of de sterren kijken.`)
                    }
                    db.incExecutions(msg.author.id).catch(err => console.error(err));
                }
            });
        } else {
            await execCmd(msg);
        }
    }
});

client.on('guildMemberAdd', (newMember) => {
    newMember.roles.add(values.verloren_materiaal_role_id);
    client.channels.fetch(values.zeg_hallo_channel_id).then(channel => {
        channel.send(newMember.toString() + " welkom op de JVS server!\n" +
            "Mogen we je naam en je kern weten (als je die hebt)? Dan zorgen we ervoor dat je een mooie rol krijgt!")
    })
});

async function execCmd(msg) {
    const cmd = msg.content.substring(1);
    if (blacklist_cmd.includes(cmd)) return;

    switch (cmd) {
        case 'ping':
            msg.reply('pong');
            break;
        case 'bot':
            msg.reply(`Ik ben een bot die ontwikkeld wordt door Arne Dierickx. Nieuwe features kunnen op deze pagina aangevraagd worden:" +
                " https://gitlab.com/pingudino98/jvsbot/-/issues.`);
            break;
        case 'dadjoke':
            request('https://icanhazdadjoke.com/', {json: true}, (err, res, body) => {
                if (err) {
                    return console.error(err);
                }
                msg.reply(body.joke);
            });
            break;
        case 'helo':
            const random = getRandomInt(1, 4);
            const heloAttachment = new Discord.MessageAttachment(`img/helo${random}.jpg`);
            msg.channel.send(heloAttachment);
            break;
        case 'sputnik':
            getRandSputnikImage(msg);
            break;
        case 'join':
            client.channels.fetch("693070672410312724")
                .then(channel => {
                    channel.join();
                });
            break;
        case 'toekomst':
            const toekomstAttachment = new Discord.MessageAttachment(`img/toekomst.png`);
            msg.channel.send(toekomstAttachment);
            break;
        case 'aankondiging':
            if (await isSTAF(msg)) repeatPinnedMessage();
            break;
        case 'xkcd':
            xkcd.random(function (err, res) {
                if (err) console.error(err);
                else {
                    msg.channel.send(res.img);
                }
            });
            break;
        case 'weetje':
            displayWeetje(msg);
            break;
        default:
            msg.reply("Dit is geen geldig commando. Nieuwe features kunnen op deze pagina aangevraagd worden:" +
                " https://gitlab.com/pingudino98/jvsbot/-/issues.");
    }
}

client.login(process.env.DISCORD_TOKEN);

function displayAPOD() {
    request(`https://api.nasa.gov/planetary/apod?api_key=${process.env.APOD_KEY}`, {json: true}, (err, res, body) => {
        if (err) {
            return console.error(err);
        }
        client.channels.fetch(values.apod_channel_id)
            .then(channel => {
                let urlName;
                if (body.hasOwnProperty('hdurl')) {
                    urlName = 'hdurl'
                } else {
                    urlName = 'url';
                }

                let attachment;
                if (body.media_type === 'video') {
                    attachment = new Discord.MessageEmbed().setTitle('Klik hier voor video').setURL(body[urlName]);
                } else {
                    attachment = new Discord.MessageAttachment(body[urlName])
                }

                channel.send(`Astronomy Picture of the Day (NASA)\n${body.title}\n${body.explanation}\nCopyright: ${body.copyright}`);
                channel.send(attachment);
            }).catch(err => console.error(err));
    });
}

function displayWeetje(msg) {
    fs.readFile('files/weetjes.txt', 'utf8', (err, data) => {
        if (err) return console.error(err);
        const lines = data.split('\n');
        const randomInt = getRandomInt(0, lines.length);
        const randomWeetje = lines[randomInt];
        msg.channel.send('Wist je dat...');
        msg.channel.send(randomWeetje);
    });
}

function cleanupVerlorenMateriaal() {
    client.guilds.fetch(values.jvs_server_id)
        .then(guild => {
            guild.roles.fetch(values.verloren_materiaal_role_id)
                .then(role => {
                    let kickedMembers = [];
                    const currDate = new Date();
                    const oneWeekAgo = currDate.getTime() - (7 * 24 * 60 * 60 * 1000);
                    role.members.forEach(member => {
                        if (member.joinedTimestamp < oneWeekAgo) {
                            kickedMembers.push(member.displayName);
                            member.kick();
                        }
                    });
                    if (kickedMembers.length !== 0) {
                        client.channels.fetch(values.staf_channel_id)
                            .then(channel => {
                                channel.send("De volgende leden werden gekicked omdat ze een week lang de rol verloren materiaal hadden.\n" +
                                kickedMembers.join(', '));
                            });
                    }
                })
                .catch(err => console.error(err));
        })
        .catch(err => console.error(err));
}

function repeatPinnedMessage() {
    client.channels.fetch(values.algemeen_channel_id)
        .then(channel => {
            channel.messages.fetchPinned()
                .then(messages => {
                    messages.forEach(message => {
                        if (message.content !== '') {
                            channel.send(message.content);
                        }
                        if (message.attachments !== null) {
                            message.attachments.forEach(attachment => {
                                channel.send(attachment);
                            })
                        }
                    });
                })
                .catch(err => console.error(err));
        })
        .catch(err => console.error(err));
}

function getRandomInt(min, max) {
    min = Math.ceil(min);
    max = Math.floor(max);
    return Math.floor(Math.random() * (max - min)) + min; //The maximum is exclusive and the minimum is inclusive
}

async function isSTAF(msg) {
    return new Promise((resolve, reject) => {
        msg.guild.roles.fetch(process.env.STAF_ROLE_ID)
            .then(role => {
                resolve(role.members.keyArray().includes(msg.author.id));
            })
            .catch(err => reject(err));
    });
}

function getRandSputnikImage(msg) {
    request('https://sputnik.jvs-descartes.org/api/observatie', {json: true}, (err, res, body) => {
        if (err) {
            return console.error(err);
        }

        const observatie = body.data[getRandomInt(body.from - 1, body.to)];
        const imagePaths = observatie.afbeeldingen;
        const images = imagePaths.map(path => `https://sputnik.jvs-descartes.org/storage/${path}`);

        msg.channel.send({files: images});
    });
}
